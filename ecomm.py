import json
import logging
import os
from functools import partial
import urllib

import tornado.auth
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import conf
from lib import utils

settings = {
    'debug': True,
    'cookie_secret': '9fe2e67d-7113-4571-aeb0-6bcc03d86b8c',
    'secure_cookie_expire_days': None,#1.0/60/60/24 * 5,
    'login_url': '/login',
}

class BaseHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.cli = tornado.httpclient.AsyncHTTPClient()

    def get_current_user(self):
        user = self.get_secure_cookie('user')
        if user:
            self.set_secure_cookie('user', user,
                expires_days=self.settings['secure_cookie_expire_days'])
        return user

class GoogleLoginHandler(tornado.web.RequestHandler,
    tornado.auth.GoogleMixin):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        if self.get_argument("openid.mode", None):
            user = yield self.get_authenticated_user()
            self.set_secure_cookie('user', json.dumps(user),
                expires_days=self.settings['secure_cookie_expire_days'])
            self.redirect(self.get_argument('next'), '/')
        else:
            yield self.authenticate_redirect()

class MainHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        user = json.loads(self.current_user)
        logging.info('{} {} accessing app.'.format(
            user['name'], user['email']))
        with open('index.html') as f:
            html = f.read()
        self.write(html)

class LocaleHandler(BaseHandler):
    def get(self, *args):
        locale = args[0]
        response = utils.csvread('locales/{}.csv'.format(locale), delimiter=',', quotechar='"')
        self.finish(dict(response))

class User(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        self.write(tornado.escape.xhtml_escape(self.current_user))

class SupportedLocalesHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        self.write(json.dumps(conf.supported_locales))

class WarehouseHandler(BaseHandler):
    def get(self):
        data = urllib2.uropen('')
        with open('static/data/warehouse.json') as f:
            data = f.read()
        self.write(data)

class QueryHandler(BaseHandler):
    @tornado.web.asynchronous
    def get(self, query, body=None):
        if query:
            if self.request.host in conf.mogno_queries_authorized_hosts:
                url = '{host}/{path}?key={key}'.format(
                    host=conf.mongo_api_host.strip('/'),
                    path=urllib.quote(query.encode('utf8')),
                    key=conf.mongo_api_key
                )
                cb = partial(self._on_response, query=query)
                if body:
                    self.cli.fetch(url, cb, method='POST', body=body)
                else:
                    self.cli.fetch(url, cb)
            else:
                self.finish({
                    'status': -1,
                    'data': '',
                    'msg': 'Unauthorized host'
                })

    def _on_response(self, response, query=None):
        response = not isinstance(response.body, basestring) and response.body or json.loads(response.body)
        if isinstance(response, dict) and 'status' in response:
            if response['status'] == 0:
                self.finish({
                    'status': 0,
                    'data': response['data'],
                    'msg': u"Query successfully executed: '{}'.".format(query)
                })
            else:
                self.finish({
                    'status': -1,
                    'data': None,
                    'msg': response['msg']
                })
        else:
            self.finish({
                'status': 0,
                'data': response,
                'msg': u"Query successfully executed: '{}'.".format(query)
            })

    @tornado.web.asynchronous
    def post(self, query):
        self.get(query, self.request.body)

application = tornado.web.Application([
    (r"/?", MainHandler),
    (r"/locale/(\w{1,2}_\w{1,2})", LocaleHandler),
    (r"/login/?", GoogleLoginHandler),
    (r"/static/(.*)", tornado.web.StaticFileHandler, {"path": os.path.join(os.path.dirname(__file__), "static")}),
    (r"/supported-locales/?", SupportedLocalesHandler),
    (r"/warehouse/?", WarehouseHandler),
    (r"/query/(.*)", QueryHandler)
], **settings)

if __name__ == "__main__":
    tornado.options.define("port", default=9997, help="run on the given port", type=int)
    tornado.options.parse_command_line()
    logging.info('Starting up')
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(tornado.options.options.port)
    tornado.autoreload.start()
    tornado.ioloop.IOLoop.instance().start()
'use strict';

/* App Module */

var ecommApp = angular.module('ecommApp', [
    'ngSanitize',
    'ngRoute',
    'ecommControllers',
    'ecommFilters',
    'ecommDirectives'
]);


ecommApp.config(['$routeProvider',
    function($routeProvider){
        $routeProvider.
            when('/', {controller: 'WarehouseCtrl', templateUrl: '/static/tpl/categories.tpl'}).
            when('/about', {controller: 'AboutCtrl', templateUrl: '/static/tpl/about.tpl'}).
            when('/contact', {controller: 'ContactCtrl', templateUrl: '/static/tpl/contact.tpl'}).
            when('/cart', {controller: 'CartCtrl', templateUrl: '/static/tpl/cart.tpl'}).
            when('/product/:productType/:productId', {controller: 'ProductCtrl', templateUrl: '/static/tpl/product.tpl'}).
            when('/checkout', {controller: 'CheckoutCtrl', templateUrl: '/static/tpl/checkout.tpl'}).
            when('/order/:orderId', {controller: 'OrderCtrl', templateUrl: '/static/tpl/order.tpl'}).
            otherwise({redirectTo: '/'});
    }]);

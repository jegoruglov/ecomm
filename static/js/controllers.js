'use strict';

/* Controllers */

var ecommControllers = angular.module('ecommControllers', ['ngCookies']);

ecommControllers.controller('MainCtrl', ['$scope', '$http', '$cookies', '$rootScope',
    function($scope, $http, $cookies, $rootScope) {
        $scope.warehouse = undefined;
        $scope.modal = {
            error: {
                msg: ''
            }
        }
        $scope.countries = ['Estonia', 'Russia', 'United States'];
        $http.get('supported-locales', {cache: true}).success(function(data){
            $scope.supported_locales = data;
        });
        $scope.products_filter = {
            product_type: 'all',
            brand: 'all'
        };
        $scope.customer = {
            billing_info: undefined,
            shipping_info: undefined
        };
        $scope.images_status = {'hidden': true};
        if($cookies.cart != undefined){
            $scope.cart = JSON.parse($cookies.cart);
        } else {
            $scope.cart = {};
            $scope.cart.items = [];
        }

        $scope.pp = function(p){
            return parseFloat(p).toFixed(2);
        }

        $scope.cartPriceTotal = function(){
            var subtotal = 0;
            $scope.cart.items.forEach(function(item){
                item.total = parseFloat(item.price) * parseInt(item.quantity);
                subtotal += item.total;
            });
            $scope.cart.subtotal = subtotal;
            $scope.cart.tax = subtotal * 0.2;
            $scope.cart.total = subtotal * 1.2;
        }
        $scope.cartPriceTotal();

        $scope.getItemsTotal = function(){
            var total = 0;
            $scope.cart.items.forEach(function(item){
                total += parseInt(item.quantity);
            });
            $scope.cart.number_of_items = total;
        }
        $scope.getItemsTotal();

        $scope.addToCart = function(item){
            var e = angular.element.makeArray(angular.element('.selections')),
                selections = {};
            if(e.length){
                e.forEach(function(i){
                    var options = []
                    angular.element.makeArray(i.options).forEach(function(o){
                        options.push(o.value);
                    });
                    selections[i.name] = {
                        options: options,
                        selected: i.value
                    }
                });
            } else {
                if(item.selects){
                    var selects = item.selects.split(',')
                    selects.forEach(function(i){
                        selections[i.trim()] = {
                            options: item[i.trim()].split(','),
                            selected: item[i.trim()].split(',')[0]
                        }
                    });
                }
            }
            item.selections = selections;
            if(angular.element('#quantity').length){
                item.quantity = angular.element('#quantity').val();    
            } else {
                item.quantity = 1;
            }
            item.total = parseFloat(item.price) * parseInt(item.quantity);
            item.total = item.total.toFixed(2);
            $scope.cart.items.push($.extend(true, {}, item));
            $scope.updateCartState();
        }

        $scope.deleteItem = function(item){
            $scope.cart.items.pop(item);
            $scope.updateCartState();
        }

        $scope.gotoCart = function(){
            window.location='#/cart';
        }
        $scope.clearCart = function(){
            $scope.cart = {};
            $scope.cart.items = [];
            $scope.updateCartState();
        }
        $scope.updateLocale = function(localeId) {
            $scope.localeId = localeId;
            $cookies.locale = localeId;
            $http.get('/locale/' + localeId, {cache: true}).success(function(data) {
                $rootScope.locale = data;
            });
        }

        $scope.updateCartState = function(){
            $scope.cartPriceTotal();
            $scope.getItemsTotal();
            $cookies.cart = JSON.stringify($scope.cart);
        }

        if($cookies.locale != undefined) {
            $scope.localeId = $cookies.locale;
        } else {
            $scope.localeId = 'en_us';
        }
        $scope.updateLocale($scope.localeId);
    }]);

ecommControllers.controller('WarehouseCtrl', ['$scope', '$http',
    function($scope, $http) {
        $http.get('query/warehouse', {cache: true}).success(function(data){
            $scope.warehouse = data.data;
        });
    }]);

ecommControllers.controller('AboutCtrl', ['$scope',
    function($scope) {

    }]);

ecommControllers.controller('ContactCtrl', ['$scope',
    function($scope) {

    }]);

ecommControllers.controller('CartCtrl', ['$scope',
    function($scope) {
        $scope.increase = function(item){
            var quantity = parseInt(item.quantity);
            quantity += 1;
            item.quantity = quantity;
            $scope.updateCartState();
        }
        $scope.decrease = function(item){
            var quantity = parseInt(item.quantity);
            quantity -= 1;
            item.quantity = quantity;
            $scope.updateCartState();
        }
    }]);

ecommControllers.controller('ProductCtrl', ['$scope', '$http', '$routeParams', '$timeout',
    function($scope, $http, $routeParams, $timeout){
        $scope.productId = $routeParams.productId;
        $scope.productType = $routeParams.productType;
        $http.get('/query/get/' + $scope.productType + '/' + $scope.productId, {cache: true}).success(function(data){
            $scope.viewed_product = data.data;
            $scope.updateImages();
        });
        $scope.updateImages = function(){
            $scope.images = undefined;
            if($scope.images_status.hidden){
                $scope.images = 'static/img/lg-image1.jpg,static/img/lg-image2.jpg,static/img/lg-image3.jpg,static/img/lg-image4.jpg';
            } else {
                $scope.images = $scope.viewed_product.images;
            }
            $scope.thumbnail = $scope.images.split(',')[0];
        }
        
        var timeoutId = undefined;

        function repeat(){
            timeoutId = $timeout(function(){
                if(angular.element('#image1') != []){
                    angular.element('#image1').zoombie();
                    $timeout.cancel(timeoutId);
                    return;
                }
                repeat();
            }, 1000);
        }
        repeat();
        $scope.updateThumbnail = function(url){
            $scope.thumbnail = url;
            angular.element('#image1').zoombie({url: url});
        }
    }]);

ecommControllers.controller('CheckoutCtrl', ['$scope', '$http',
    function($scope, $http) {
        $scope.same_as_billing = true;
        if($scope.customer.billing_info == undefined){
            $scope.customer.billing_info = {country: 'Estonia'};
        }
        if($scope.customer.shipping_info == undefined){
            $scope.customer.shipping_info = {country: 'Estonia'};
        }
        $scope.cancelShippingInfo = function(){
            if(!$scope.same_as_billing){
                $scope.customer.shipping_info = {country: 'Estonia'};
            }
        }
        $scope.checkout = function(){
            $scope.checkoutForm.submitted = true;
            if($scope.same_as_billing){
                $scope.customer.shipping_info = $scope.customer.billing_info;
            }
            if($scope.checkoutForm.$valid){
                $scope.customer.shipping_same_as_billing = $scope.same_as_billing;
                angular.element('#checkout-btn').button('loading');
                $scope.order = {
                    cart: $scope.cart,
                    customer: $scope.customer
                }
                $http.post('query/checkout', $scope.order).success(function(data){
                    $scope.clearCart();
                    window.location = '#/order/' + data.data;
                }).error(function(data){
                    $scope.modal.error.msg = data;
                    angular.element('#modal-error').modal('show');
                    angular.element('#checkout-btn').button('reset');
                });
            } else {
                $scope.modal.error.msg = '<p>Some required fields are missing or filled incorrectly.</p>' +
                    '<p>Please refer to <span class="red">red</span> markers.</p>';
                angular.element('#modal-error').modal('show');
                angular.element('#checkout-btn').button('reset');
            }
        }
    }]);

ecommControllers.controller('OrderCtrl', ['$scope', '$http', '$routeParams',
    function($scope, $http, $routeParams){
        $scope.orderId = $routeParams.orderId;
        $http.get('query/order/' + $scope.orderId).success(function(data){
            $scope.confirmed_order = data.data;
        });
    }]);
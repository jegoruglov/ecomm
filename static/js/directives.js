'use strict';

/* Directives */

var ecommDirectives = angular.module('ecommDirectives', []);

var aspects = function(item){
    var html = '';
    if(item.product_type == 'pads'){
        if(item.speed != ''){
            html +=
            '<span class="aspect first">Speed<i class="icon-speed"></i><span class="aspect-number">' + item.speed + '</span></span>' +
            '<span class="aspect">Spin<i class="icon-spin"></i><span class="aspect-number">' + item.spin + '</span></span>' +
            '<span class="aspect">Control<i class="icon-control"></i><span class="aspect-number">' + item.control + '</span></span>';
        } else {
            html += '<p style="height: 33px"></p>';
        }
    } else if(item.product_type == 'rubber'){
        html +=
        '<span class="aspect first">Approved by <span class="aspect-text">' + item.approved + '</span></span>' +
        '<span class="aspect">Recipe <span class="aspect-text">' + item.recipe + '</span></span>';
    } else if(item.product_type == 'blades'){
        if(item.speed != ''){
            html +=
            '<span class="aspect first">Speed<i class="icon-speed"></i><span class="aspect-number">' + item.speed + '</span></span>' +
            '<span class="aspect">Control<i class="icon-control"></i><span class="aspect-number">' + item.control + '</span></span>';
        } else {
            html += '<p style="height: 33px"></p>';
        }
    } else if(item.product_type == 'sets'){
         html +=
         '<span class="aspect first">' + item.content + '</span>';
    } else if(item.product_type == 'balls'){
         html +=
         '<span class="aspect first">Quantity <span class="aspect-number">' + item.quantity + '</span></span>' +
         '<span class="aspect">Diameter <span class="aspect-number">' + item.size + '</span></span>';
    } else if(item.product_type == 'covers'){
         html += '<p style="height: 33px"></p>';
    } else {
        html = '';
    }
    return html;
}

ecommDirectives.directive('rowGenerator', function($compile) {
    var rowTemplate = '<div class="row products-row">',
        colTemplate = '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">';
    return {
        restrict: 'E',
        // use '=' for colCount instead of '@' so that we don't
        // have to use attr.$observe() in the link function
        controller: function($scope, $element, $attrs){
            $scope.colCount = $attrs.colCount;
        },
        link: function(scope, element, attrs) {
            // To save CPU time, we'll just watch the overall
            // length of the rowData array for changes.  You
            // may need to watch more.
            scope.$watch('[images_status,warehouse,products_filter]', function(value) {
                var count = 0,
                    lorempixel_count = 199,
                    html = rowTemplate;
                if(scope.warehouse){
                    for(var i=0; i < scope.warehouse.length; i++) {
                        if(scope.products_filter.brand != 'all' && scope.warehouse[i].brand != scope.products_filter.brand){
                            continue;
                        }
                        if(scope.products_filter.product_type != 'all' && scope.warehouse[i].product_type != scope.products_filter.product_type){
                            continue;
                        }
                        count += 1;
                        lorempixel_count += 1;
                        var description = undefined;
                        if(scope.images_status.hidden){
                            description = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'
                        } else {
                            description = scope.warehouse[i].description;
                        }
                        var img_url = undefined;
                        if(!scope.images_status.hidden) {
                            if(scope.warehouse[i].images){
                                img_url = scope.warehouse[i].images.split(',')[0];
                            } else {
                                img_url = 'http://placehold.it/500x200';
                            }
                        } else {
                            img_url = 'http://lorempixel.com/500/' + lorempixel_count;
                        }
                        var img = '<img src="' + img_url + '" style="max-width:100%; max-height:100%;">';
                        html += colTemplate + 
                        '<div class="thumbnail">' +
                            '<a href="#/product/' + scope.warehouse[i].product_type + '/' + scope.warehouse[i]._id.$oid + '">' + img + '</a> ' +
                            '<div class="caption">' +
                                '<h3>' + scope.warehouse[i].model + '</h3>' +
                                '<p>' + description + '</p>' +
                                '<hr>' +
                                '<p>' + aspects(scope.warehouse[i]) + '</p>' +
                                '<hr>' +
                                '<p>' +
                                    '<a class="btn btn-primary" href="#/product/' + scope.warehouse[i].product_type + '/' + scope.warehouse[i]._id.$oid + '">View details</a> ' +
                                    "<a class='btn btn-success' href='' data-ng-click='addToCart(" + JSON.stringify(scope.warehouse[i]) + ")'>" +
                                        '<i class="icon-shopping-cart"></i>Add to cart' +
                                    '</a> ' +
                                    '<span class="price">' + scope.warehouse[i].price + ' &euro;</span>' +
                                '</p>' +
                            '</div>' +
                        '</div>' +
                        '</div>';
                        if (count % scope.colCount == 0) {
                            html += '</div>' + rowTemplate;
                        }
                    }
                }
                html += '</div>';
                // don't use replaceWith() as the $watch 
                // above will not work -- use html() instead
                html = $compile(html)(scope);
                element.html(html);
            }, true);
        }
    }
});

var mindSelects = function(label, s){
    var html = '',
        selects = s.split(',');
    if(selects.length > 1){
        html = '<div class="form-horizontal">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label" style="text-align:left;font-weight: 200">' +
            label + ': </label>';
        html += '<select class="form-control input-xs selections" name="' + label + '" style="width:150px">'
        angular.forEach(selects, function(c){
            html +=
                '<option>' + c.trim() + '</option>';
        });
        html += '</select></div></div>';
    } else {
        html += '<p>' + label + ': ' + s + '</p>';
    }
    return html
}

ecommDirectives.directive('productprops', function(){
    return {
        restrict: 'E',
        scope: {product: '=', productType: '='},
        link: function(scope, element, attrs){
            scope.$watch('[product,productType]', function(value){
                if(value[0]){
                    var html = '<div class="aspect">',
                        product = value[0];
                    product.product_type = value[1];
                    if(product.product_type == 'pads'){
                        html +=
                            '<p>' + aspects(product) + '</p>' +
                            '<p>Blade: ' + product.blade + '</p>' +
                            '<p>Rubber: ' + product.rubber + '</p>' +
                            '<p>Sponge: ' + product.sponge + '</p>' +
                            '<p>Print/Lens: ' + product['print/lens'] + '</p>' +
                            '<p>Handle: ' + product.handle + '</p>' +
                            '<p>Packing: ' + product.packing + '</p>' +
                            '<p style="font-weight: 500;">Shipping info: ' + 'XXX' + '</p>';
                    } else if(product.product_type == 'blades'){
                        html +=
                            '<p>' + aspects(product) + '</p>' +
                            '<p>Weight: ' + product.weight + '</p>' +
                            '<p>Blade: ' + product.blade + '</p>' +
                            '<p>Print/lens: ' + product['print/lens'] + '</p>' +
                            '<p>Handle: ' + product.handle + '</p>' +
                            '<p>Packaging: ' + product.packaging + '</p>' +
                            '<p style="font-weight: 500;">Shipping info: ' + 'XXX' + '</p>';
                    } else if(product.product_type == 'rubber'){
                        html +=
                            '<p>' + aspects(product) + '</p>' +
                            '<p>Rubber: ' + product.rubber + '</p>' +
                            mindSelects('Sponge', product.sponge) +
                            '<p>Paper protection: ' + product['paper-protection'] + '</p>' +
                            '<p>Packaging: ' + product.packaging + '</p>' +
                            '<p style="font-weight: 500;">Shipping info: ' + 'XXX' + '</p>';
                    } else if(product.product_type == 'balls'){
                        html +=
                            '<p>' + aspects(product) + '</p>' +
                            mindSelects('Color', product.colour) +
                            '<p style="font-weight: 500;">Shipping info: ' + 'XXX' + '</p>';
                    } else if(product.product_type == 'covers'){
                        html +=
                            '<p>' + aspects(product) + '</p>' +
                            '<p>Quality: ' + product.quality + '</p>' +
                            mindSelects('Color', product.colour) +
                            '<p style="font-weight: 500;">Shipping info: ' + 'XXX' + '</p>';
                    } else if(product.product_type == 'sets'){
                        html +=
                            '<p>' + aspects(product) + '</p>' +
                            '<p style="font-weight: 500;">Shipping info: ' + 'XXX' + '</p>';
                    }
                    html += '<div class="form-horizontal">' +
                        '<div class="form-group">' +
                            '<label class="control-label" style="text-align:left;font-weight: 200">' +
                                'How many of "' + product.model + '" ' + product.product_type + ' you would like to order? ' +
                            '</label>' +
                            '<input id="quantity" type="number" value="1" min="1" class="input-xs" name="Quantity" style="width: 60px;text-align:right; font-size:14px; margin-left:20px"/>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label" style="text-align:left;font-weight: 200">' +
                                'Do you have a voucher code: ' +
                            '</label>' +
                            '<input type="text" class="input-xs" style="margin-left:20px; font-size:14px"/>' +
                        '</div>' +
                    '</div>';
                    element.html(html);
                }
            }, true);
        }
    }
})
ecommDirectives.directive('testdir', function(){
    return {
        restrict: 'E',
        controller: function($scope, $element, $attrs){
            $scope.aaa = $attrs.aaa;
        },
        template: '<input data-ng-model="hei.hei">{{aaa}}'
    }
});
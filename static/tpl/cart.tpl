<div class="row" style="font-weight:200">
  <div class="col-lg-12 table-responsive">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Cart content</h3>
      </div>
      <div class="panel-body">
        <table class="table table-hover table-condensed">
          <thead>
            <tr>
              <th></th>
              <th></th>
              <th></th>
              <th>Quantity</th>
              <th>Unit price</th>
              <th>Total</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            <tr data-ng-repeat="item in cart.items track by $index">
              <td><a data-ng-href="#/product/{{item.product_type}}/{{item._id.$oid}}"><img data-ng-src="{{item.images.split(',')[0]}}" style="width: 50px"/></a></td>
              <td><a data-ng-href="#/product/{{item.product_type}}/{{item._id.$oid}}"><span style="font-size:16px">{{item.model}}</span></a></td>
              <td>
                <form class="form-inline" role="form">
                  <div class="form-group" data-ng-repeat="(k, v) in item.selections">
                    <label for="exampleInputEmail1">{{k}}</label>
                    <select class="form-control input-sm" data-ng-options="option for option in v.options" data-ng-model="v.selected" data-ng-change="updateCartState()"></select>
                  </div>
                </form>
              </td>
              <td>
                <div class="input-group input-group-sm" style="width: 90px">
                  <span class="input-group-btn">
                    <button class="btn btn-default" style="padding-left:2px;padding-right:2px" type="button" data-ng-click="increase(item)"><i class="icon-up"></i></button>
                  </span>
                  <input type="text" data-ng-model="item.quantity" class="form-control" style="text-align:right" data-ng-change="updateCartState()">
                  <span class="input-group-btn">
                    <button class="btn btn-default" style="padding-left:2px;padding-right:2px" type="button" data-ng-click="decrease(item)"><i class="icon-down"></i></button>
                  </span>
                </div>
              </td>
              <td>{{pp(item.price)}} &euro;</td>
              <td>{{pp(item.total)}} &euro;</td>
              <td><a href="" data-ng-click="deleteItem(item)" class="btn btn-default btn-sm"><i class="icon-trash"></i></a></td>
            </tr>
          </tbody>
        </table>
        <hr>
        <div style="float:right; clear:both">
          <table style="text-align: right; float:right">
            <thead>
              <tr>
                <th></th>
                <th style="width:100px"></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><h5>Subtotal:</h5></td>
                <td><h5>{{pp(cart.subtotal)}} &euro;</h5></td>
              </tr>
              <tr>
                <td><h5>Tax:</h5></td>
                <td><h5>{{pp(cart.tax)}} &euro;</h5></td>
              </tr>
              <tr>
                <td><h4>Total:</h4></td>
                <td><h4>{{pp(cart.total)}} &euro;</h4></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="btn-group btn-group-justified" style="margin-top:10px; margin-bottom:10px; clear:both">
  <a class="btn btn-primary" href="#"><i class="icon-back"></i>Back to shop</a>
  <a class="btn btn-danger" href="#" data-ng-click="clearCart()"><i class="icon-trash"></i>Clear cart</a>
  <a class="btn btn-warning" href="#/checkout">Checkout</a>
</div>

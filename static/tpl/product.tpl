<div class="row">
  <div class="col-lg-6">
    <span class='zoom' id='image1'>
      <img data-ng-src="{{thumbnail}}" id='img1' style="border: 2px solid #666666; max-width: 100%;" alt='Daisy on the Ohoopee'/>
    </span>
    <div class="row">
      <div class="col-lg-3">
        <img  data-ng-mouseover="updateThumbnail(images.split(',')[0])" data-ng-src="{{images.split(',')[0]}}" id='img1' style="border: 2px solid #666666; max-width: 100%;" alt='Daisy on the Ohoopee'/>
      </div>
      <div class="col-lg-3">
        <img data-ng-mouseover="updateThumbnail(images.split(',')[1])" data-ng-src="{{images.split(',')[1]}}" id='img1' style="border: 2px solid #666666; max-width: 100%;" alt='Daisy on the Ohoopee'/>
      </div>
      <div class="col-lg-3">
        <img data-ng-mouseover="updateThumbnail(images.split(',')[2])" data-ng-src="{{images.split(',')[2]}}" id='img1' style="border: 2px solid #666666; max-width: 100%;" alt='Daisy on the Ohoopee'/>
      </div>
      <div class="col-lg-3">
        <img data-ng-mouseover="updateThumbnail(images.split(',')[3])" data-ng-src="{{images.split(',')[3]}}" id='img1' style="border: 2px solid #666666; max-width: 100%;" alt='Daisy on the Ohoopee'/>
      </div>
    </div>
  </div>
  <div class="col-lg-6">
    <h3>{{viewed_product.model}}</h3>
    <p data-ng-if="!images_status.hidden">{{viewed_product.description}}</p>
    <p data-ng-if="images_status.hidden">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
    <productprops product="viewed_product" product-type="productType"></productprops>
    <p class="product-price">{{viewed_product.price}} &euro;</p>
  </div>
</div>
<div class="btn-group btn-group-justified" style="margin-top:10px; margin-bottom:10px">
  <a class="btn btn-primary" href="#"><i class="icon-back"></i>Back to shop</a>
  <a class="btn btn-success" href="" data-ng-click="addToCart(viewed_product)"><i class="icon-shopping-cart"></i>Add to cart</a>
  <a class="btn btn-warning" href="#/cart">Checkout</a>
</div>
<div class="row">
  <div class="col-lg-6">
    <h1>{{locale.about_h}}</h1>
    <p class="lead">{{locale.about_p}}</p>
    <a class="btn btn-large btn-success" href="#">{{locale.jumbotron_go_shopping}} &raquo;</a>
  </div>
  <div class="col-lg-6">
    <div id="carousel-example-generic" class="carousel slide">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <img src="/static/img/plywood.jpg" alt="Plywood">
          <div class="carousel-caption">
            <div class="carousel-caption-background">
              <h3>{{locale.plywood_h}}</h3>
              <p >{{locale.plywood_p}}</p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="/static/img/blades.jpg" alt="Blades">
          <div class="carousel-caption">
            <div class="carousel-caption-background">
              <h3>{{locale.blades_h}}</h3>
              <p>{{locale.blades_p}}</p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="/static/img/rubber.jpg" alt="Rubber">
          <div class="carousel-caption">
            <div class="carousel-caption-background">
              <h3>{{locale.rubber_h}}</h3>
              <p>{{locale.rubber_p}}</p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="/static/img/handles.jpg" alt="Handles">
          <div class="carousel-caption">
            <div class="carousel-caption-background">
              <h3>{{locale.handles_h}}</h3>
              <p>{{locale.handles_p}}</p>
            </div>
          </div>
        </div>
      </div>

      <!-- Controls -->
      <a class="left carousel-control" data-target="#carousel-example-generic" data-slide="prev">
        <span class="icon-prev"></span>
      </a>
      <a class="right carousel-control" data-target="#carousel-example-generic" data-slide="next">
        <span class="icon-next"></span>
      </a>
    </div>
  </div>
</div>

<hr>

<div class="row">
  <div class="col-lg-6 col-md-6 col-sm-6">
    <h4>{{locale.about_a_h}}</h4>
    <p>{{locale.about_a_p}}</p>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6">
    <h4>{{locale.about_b_h}}</h4>
    <p>{{locale.about_b_p}}</p>
  </div>
</div>
<div class="row">
  <div class="col-lg-6 col-md-6 col-sm-6">
    <h4>{{locale.about_c_h}}</h4>
    <p>{{locale.about_c_p}}</p>
  </div>
  <div class="col-lg-6 col-md-6 col-ms-6">
    <h4>{{locale.about_d_h}}</h4>
    <p>{{locale.about_d_p}}</p>
  </div>
</div>
<div class="row">
  <div class="col-lg-6 col-md-6 col-sm-6">
    <h4>{{locale.about_e_h}}</h4>
    <p>{{locale.about_e_p}}</p>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6">
    <h4>{{locale.about_f_h}}</h4>
    <p>{{locale.about_f_p}}</p>
  </div>
</div>

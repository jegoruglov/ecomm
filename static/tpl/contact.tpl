<!-- Main hero unit for a primary marketing message or call to action -->
<div class="row" style="margin-top:40px">
  <div class="col-lg-10 col-lg-offset-1">
    <div class="jumbotron" style="background: url('static/img/BeFunky_map3.jpg');background-repeat:no-pereat;background-size:100% 100%;max-width:961px;max-height:260px">
      <div class="container">
        <h1>{{locale.location_h}}</h1>
        <p>{{locale.location_p}}</p>
        <p><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Kerese+38+a,+narva,+estonia&amp;aq=&amp;sll=59.374272,28.159761&amp;sspn=0.009488,0.031629&amp;ie=UTF8&amp;hq=&amp;hnear=Paul+Kerese+38,+21004+Narva,+Estonia&amp;t=m&amp;ll=60.413852,29.179688&amp;spn=31.071983,74.707031&amp;z=3&amp;iwloc=A" target="_blank" class="btn btn-success btn-large">View Map  &raquo; &raquo;</a></p>
      </div>
    </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-lg-2 col-lg-offset-2">
    <h3>Contacts</h3>
    <address>
      <strong>AS NTT</strong><br>
      Kerese 38 a<br>
      21003 Narva<br>
      Estonia<br>
      <abbr title="Phone">P:</abbr> +372 35 66 384<br>
      <abbr title="Fax">F:</abbr> +372 35 66 380<br>
      <abbr title="Email">E:</abbr> <a href="mailto:#">nttsale@hot.ee</a>
    </address>

    <address>
      <strong>Oleg Uglov</strong><br>
      President<br>
      <a href="mailto:#">nttuglov@hot.ee</a>
    </address>

    <address>
      <strong>Roman Babinets</strong><br>
      General Manager<br>
      +372 555 10 122<br>
      <a href="mailto:#">babinets@hot.ee</a>
    </address>

    <address>
      <strong>Anastassia Jakovleva</strong><br>
      Export Manager<br>
      +372 51 29 225<br>
      <a href="mailto:#">nttsale@hot.ee</a>
    </address>

    <address>
      <strong>Svetlana Egorova</strong><br>
      Purchase Manager<br>
      +372 555 10 121<br>
      <a href="mailto:#">svt@hot.ee</a>
    </address>

  </div>
  <div class="col-lg-5 col-lg-offset-1">
    <h3>Request form</h3>
    <form role="form">
      <div class="form-group">
        <label for="exampleInputPassword1">Name</label>
        <input type="text" class="form-control" id="exampleInputName1" placeholder="Your name">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Your email">
      </div>
      <div class="form-group">
      <label for="exampleInputText1">Request</label>
        <textarea class="form-control" rows="3" placeholder="Your request"></textarea>
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
  </div>
</div>
	<div data-ng-if="confirmed_order">
	<div class="row" style="margin-top:40px">
		<div class="col-lg-5">
			<span style="font-size: 24px">Order nr.</span> <span style="font-size: 22px; font-weight: 200">{{confirmed_order._id.$oid}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-5 col-lg-offset-7">
			<address style="text-align:right">
				<strong>{{confirmed_order.customer.billing_info.company_name}}</strong><br data-ng-if="confirmed_order.customer.billing_info.company_name">
				<strong>{{confirmed_order.customer.billing_info.first_name}} {{confirmed_order.customer.billing_info.last_name}}</strong><br data-ng-if="confirmed_order.customer.billing_info.first_name || confirmed_order.customer.billing_info.last_name">
				{{confirmed_order.customer.billing_info.address}}<br data-ng-if="confirmed_order.customer.billing_info.address">
				{{confirmed_order.customer.billing_info.address_line2}}<br data-ng-if="confirmed_order.customer.billing_info.address_line2">
				{{confirmed_order.customer.billing_info.city}}<br data-ng-if="confirmed_order.customer.billing_info.city">
				{{confirmed_order.customer.billing_info.state}}<br data-ng-if="confirmed_order.customer.billing_info.state">
				{{confirmed_order.customer.billing_info.country}}<br data-ng-if="confirmed_order.customer.billing_info.country">
				{{confirmed_order.customer.billing_info.post_code}}<br data-ng-if="confirmed_order.customer.billing_info.post_code">
				{{confirmed_order.customer.billing_info.email}}<br data-ng-if="confirmed_order.customer.billing_info.email">
				{{confirmed_order.customer.billing_info.phone}}<br data-ng-if="confirmed_order.customer.billing_info.phone">
			</address>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<table class="table table-hover table-condensed borders-off">
				<thead>
					<tr>
						<th>Product</th>
						<th>Quantity</th>
						<th style="text-align:right">Total</th>
					</tr>
				</thead>
				<tbody>
					<tr data-ng-repeat="item in confirmed_order.cart.items track by $index">
						<td>
							<p>{{item.model}}</p>
							<p data-ng-repeat="(k, v) in item.selections" style="font-size:11px">
								{{k}}: {{v.selected}}
							</p>
						</td>
						<td>{{item.quantity}}</td>
						<td style="text-align:right">{{pp(item.total)}} &euro;</td>
					</tr>
				</tbody>
			</table>
			<hr>
			<table class="table table-condensed borders-off" style="text-align:right">
				<thead>
					<tr>
						<td style="width:40%"></td>
						<td></td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td></td>
						<td><span style="font-size: 12px; font-weight:500">Subtotal:</span></td>
						<td><span style="font-size: 12px; font-weight:500">{{pp(confirmed_order.cart.subtotal)}} &euro;</span></td>
					</tr>
					<tr>
						<td></td>
						<td><span style="font-size: 12px; font-weight:500">Tax:</span></td>
						<td><span style="font-size: 12px; font-weight:500">{{pp(confirmed_order.cart.tax)}} &euro;</span></td>
					</tr>
					<tr>
						<td></td>
						<td><span style="font-size: 14px; font-weight:500">Total:</span></td>
						<td><span style="font-size: 14px; font-weight:500">{{pp(confirmed_order.cart.total)}} &euro;</span></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
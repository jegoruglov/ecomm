<div class="row">
	<form name="checkoutForm" class="css-form">
		<div class="col-lg-8">
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">Billing information</h3>
			  </div>
			  <div class="panel-body">
			    <div class="row">
					<div class="col-lg-4">
						<div role="form">
							<div class="form-group">
							  	<label for="inputFirstName">First name</label>
							  	<span class="error" ng-show="checkoutForm.billingFirstName.$error.required && checkoutForm.submitted">Required</span>
							    <input data-ng-model="customer.billing_info.first_name" name="billingFirstName" type="text" class="form-control input-sm" id="inputFirstName" placeholder="First name" tabindex="1" autocomplete="on" required>
							</div>
							<div class="form-group">
							    <label for="inputCompanyName">Company name</label>
							    <input data-ng-model="customer.billing_info.company_name" type="text" class="form-control input-sm" id="inputCompanyName" placeholder="Company name" tabindex="4" autocomplete="on">
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div role="form">
							<div class="form-group">
					  			<label for="inputLastName">Last name</label>
					  			<span class="error" ng-show="checkoutForm.billingLastName.$error.required && checkoutForm.submitted">Required</span>
					    		<input data-ng-model="customer.billing_info.last_name" name="billingLastName" type="text" class="form-control input-sm" id="inputLastName" placeholder="Last name" tabindex="2" autocomplete="on" required>
							</div>
							<div class="form-group">
					    		<label for="inputVatId">VAT ID</label>
					    		<input data-ng-model="customer.billing_info.vat_id" type="text" class="form-control input-sm" id="inputVatId" placeholder="VAT ID" tabindex="5" autocomplete="on">
							</div>
							
						</div>
					</div>
					<div class="col-lg-4">
						<div role="form">
							<div class="form-group">
					    		<label for="inputEmail">Email address</label>
					    		<span class="error" ng-show="checkoutForm.billingEmail.$error.required && checkoutForm.submitted">Required</span>
					    		<span class="error" ng-show="checkoutForm.billingEmail.$error.email && checkoutForm.submitted">Invalid email</span>
					    		<input data-ng-model="customer.billing_info.email" name="billingEmail" type="email" class="form-control input-sm" id="inputEmail" placeholder="Email address" tabindex="3" autocomplete="on" required>
							</div>
							<div class="form-group">
							    <label for="inputPhoneNumber">Phone number</label>
							    <input data-ng-model="customer.billing_info.phone" type="tel" class="form-control input-sm" id="inputPhoneNumber" placeholder="Phone number" tabindex="6" autocomplete="on">
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-lg-4">
						<div role="form">
							<div class="form-group">
							    <label for="inputAddress">Address</label>
							    <span class="error" ng-show="checkoutForm.billingAddress.$error.required && checkoutForm.submitted">Required</span>
							    <input data-ng-model="customer.billing_info.address" name="billingAddress" type="text" class="form-control input-sm" id="inputAddress" placeholder="Address" tabindex="7" autocomplete="on" required>
							</div>
							<div class="form-group">
							    <label for="inputCountry">Country</label>
							    <span class="error" ng-show="checkoutForm.billingCountry.$error.required && checkoutForm.submitted">Required</span>
							    <select data-ng-model="customer.billing_info.country" name="billingCountry" class="form-control input-sm" id="inputCountry" tabindex="10" autocomplete="on" required data-ng-options="c for c in countries">
							    </select>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div role="form">
							<div class="form-group">
							    <label for="inputAddressLine2">Address line 2</label>
							    <input data-ng-model="customer.billing_info.address_line2" type="text" class="form-control input-sm" id="inputAddressLine2" placeholder="Address line 2" tabindex="8" autocomplete="on">
							</div>
							<div class="form-group">
							    <label for="inputState">State</label>
							    <span class="error" ng-show="checkoutForm.billingState.$error.required && checkoutForm.submitted">Required</span>
							    <input data-ng-model="customer.billing_info.state" name="billingState" type="text" class="form-control input-sm" id="inputState" placeholder="State" tabindex="11" autocomplete="on" data-ng-disabled="customer.billing_info.country != 'United States'" data-ng-required="customer.billing_info.country == 'United States'">
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div role="form">
							<div class="form-group">
							    <label for="inputCity">City</label>
							    <span class="error" ng-show="checkoutForm.billingCity.$error.required && checkoutForm.submitted">Required</span>
							    <input data-ng-model="customer.billing_info.city" name="billingCity" type="text" class="form-control input-sm" id="inputCity" placeholder="City" tabindex="9" autocomplete="on" required>
							</div>
							<div class="form-group">
							    <label for="inputPostCode">Post code</label>
							    <span class="error" ng-show="checkoutForm.billingPostCode.$error.required && checkoutForm.submitted">Required</span>
							    <input data-ng-model="customer.billing_info.post_code" name="billingPostCode" type="text" class="form-control input-sm" id="inputPostCode" placeholder="Post code" tabindex="12" autocomplete="on" required>
							</div>
						</div>
					</div>
				</div>
			  </div>
			</div>
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <span class="panel-title">Shipping information</span><span class="pull-right" data-ng-click="cancelShippingInfo()"><input type="checkbox" data-ng-model="same_as_billing"> Same as Billing</span>
			  </div>
			  <div class="panel-body">
			  	<div data-ng-if="!same_as_billing">
				    <div class="row">
						<div class="col-lg-4">
							<div role="form">
								<div class="form-group">
								  	<label for="inputFirstName">First name</label>
								  	<span class="error" ng-show="checkoutForm.shippingFirstName.$error.required && checkoutForm.submitted">Required</span>
								    <input data-ng-model="customer.shipping_info.first_name" name="shippingFirstName" type="text" class="form-control input-sm" id="inputFirstName" placeholder="First name" tabindex="21" autocomplete="on" required>
								</div>
								<div class="form-group">
								    <label for="inputCompanyName">Company name</label>
								    <input data-ng-model="customer.shipping_info.company" type="text" class="form-control input-sm" id="inputCompanyName" placeholder="Company name" tabindex="24" autocomplete="on">
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div role="form">
								<div class="form-group">
						  			<label for="inputLastName">Last name</label>
						  			<span class="error" ng-show="checkoutForm.shippingLastName.$error.required && checkoutForm.submitted">Required</span>
						    		<input data-ng-model="customer.shipping_info.last_name" name="shippingLastName" type="text" class="form-control input-sm" id="inputLastName" placeholder="Last name" tabindex="22" autocomplete="on" required>
								</div>
								<div class="form-group">
						    		<label for="inputVatId">VAT ID</label>
						    		<input data-ng-model="customer.shipping_info.vat_id" type="text" class="form-control input-sm" id="inputVatId" placeholder="VAT ID" tabindex="25" autocomplete="on">
								</div>
								
							</div>
						</div>
						<div class="col-lg-4">
							<div role="form">
								<div class="form-group">
						    		<label for="inputEmail">Email address</label>
						    		<span class="error" ng-show="checkoutForm.shippingEmail.$error.required && checkoutForm.submitted">Required</span>
						    		<span class="error" ng-show="checkoutForm.shippingEmail.$error.email && checkoutForm.submitted">Invalid email</span>
						    		<input data-ng-model="customer.shipping_info.email" name="shippingEmail" type="email" class="form-control input-sm" id="inputEmail" placeholder="Email address" tabindex="23" autocomplete="on" required>
								</div>
								<div class="form-group">
								    <label for="inputPhoneNumber">Phone number</label>
								    <input data-ng-model="customer.shipping_info.phone" type="tel" class="form-control input-sm" id="inputPhoneNumber" placeholder="Phone number" tabindex="26" autocomplete="on">
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-lg-4">
							<div role="form">
								<div class="form-group">
								    <label for="inputAddress">Address</label>
								    <span class="error" ng-show="checkoutForm.shippingAddress.$error.required && checkoutForm.submitted">Required</span>
								    <input data-ng-model="customer.shipping_info.address" name="shippingAddress" type="text" class="form-control input-sm" id="inputAddress" placeholder="Address" tabindex="27" autocomplete="on" required>
								</div>
								<div class="form-group">
								    <label for="inputCountry">Country</label>
								    <span class="error" ng-show="checkoutForm.shippingCountry.$error.required && checkoutForm.submitted">Required</span>
								    <select data-ng-model="customer.shipping_info.country" name="shippingCountry" class="form-control input-sm" id="inputCountry" tabindex="110" autocomplete="on" required>
								    	<option selected="selected">Estonia</option>
								    	<option>United States</option>
								    </select>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div role="form">
								<div class="form-group">
								    <label for="inputAddressLine2">Address line 2</label>
								    <input data-ng-model="customer.shipping_info.address_line2" type="text" class="form-control input-sm" id="inputAddressLine2" placeholder="Address line 2" tabindex="28" autocomplete="on">
								</div>
								<div class="form-group">
								    <label for="inputState">State</label>
								    <span class="error" ng-show="checkoutForm.shippingState.$error.required && checkoutForm.submitted">Required</span>
								    <input data-ng-model="customer.shipping_info.state" name="shippingState" type="text" class="form-control input-sm" id="inputState" placeholder="State" tabindex="211" autocomplete="on" data-ng-disabled="customer.shipping_info.country != 'United States'" data-ng-required="customer.shipping_info.country == 'United States'">
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div role="form">
								<div class="form-group">
								    <label for="inputCity">City</label>
								    <span class="error" ng-show="checkoutForm.shippingCity.$error.required && checkoutForm.submitted">Required</span>
								    <input data-ng-model="customer.shipping_info.city" name="shippingCity" type="text" class="form-control input-sm" id="inputCity" placeholder="City" tabindex="29" autocomplete="on" required>
								</div>
								<div class="form-group">
								    <label for="inputPostCode">Post code</label>
								    <span class="error" ng-show="checkoutForm.shippingPostCode.$error.required && checkoutForm.submitted">Required</span>
								    <input data-ng-model="customer.shipping_info.post_code" name="shippingPostCode" type="text" class="form-control input-sm" id="inputPostCode" placeholder="Post code" tabindex="212" autocomplete="on" required>
								</div>
							</div>
						</div>
					</div>
				</div>
			  </div>
			</div>
		</div>
	</form>
	<div class="col-lg-4">
		<div class="panel panel-default">
		  <div class="panel-heading">
		    <h3 class="panel-title">Cart content</h3>
		  </div>
		  <div class="panel-body" style="font-size: 12px;">
		    <table class="table table-hover table-condensed borders-off">
				<thead>
					<tr>
						<th>Product</th>
						<th>Quantity</th>
						<th style="text-align:right">Total</th>
					</tr>
				</thead>
				<tbody>
					<tr data-ng-repeat="item in cart.items track by $index">
						<td>
							<p>{{item.model}}</p>
							<p data-ng-repeat="(k, v) in item.selections" style="font-size:11px">
								{{k}}: {{v.selected}}
							</p>
						</td>
						<td>{{item.quantity}}</td>
						<td style="text-align:right">{{pp(item.total)}} &euro;</td>
					</tr>
				</tbody>
			</table>
			<hr>
			<table class="table table-condensed borders-off" style="text-align:right">
				<thead>
					<tr>
						<td style="width:40%"></td>
						<td></td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td></td>
						<td><span style="font-size: 12px; font-weight:500">Subtotal:</span></td>
						<td><span style="font-size: 12px; font-weight:500">{{pp(cart.subtotal)}} &euro;</span></td>
					</tr>
					<tr>
						<td></td>
						<td><span style="font-size: 12px; font-weight:500">Tax:</span></td>
						<td><span style="font-size: 12px; font-weight:500">{{pp(cart.tax)}} &euro;</span></td>
					</tr>
					<tr>
						<td></td>
						<td><span style="font-size: 14px; font-weight:500">Total:</span></td>
						<td><span style="font-size: 14px; font-weight:500">{{pp(cart.total)}} &euro;</span></td>
					</tr>
				</tbody>
			</table>
		  </div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="btn-group btn-group-justified">
		  <a class="btn btn-primary" href="#"><i class="icon-back"></i>Back to shop</a>
		  <a class="btn btn-danger" href="#/cart"><i class="icon-back"></i>Back to cart</a>
		  <a class="btn btn-warning" id="checkout-btn" href="" data-ng-click="checkout()" data-loading-text="Loading...">Checkout</a>
		</div>
	</div>
</div>
<!-- Main hero unit for a primary marketing message or call to action -->

<a href="" class="btn btn-danger" data-ng-click="images_status.hidden = !images_status.hidden" style="position:fixed; top: 8px;left: 10px;z-index:9999">Toggle DEMO</a>

<div class="jumbotron visible-md visible-lg">
	<div class="container">
	  <h1>{{locale.hero_header}}</h1>
	  <p>{{locale.hero_desc}}</p>
	  <p><a href="#/about" class="btn btn-primary btn-large">{{locale.hero_btn}} &raquo;</a></p>
	</div>
</div>
<!-- Example row of columns -->
<row-generator col-count="3"></row-generator>